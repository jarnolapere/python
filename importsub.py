import subprocess
with open("hosts_and_ports.txt") as hp_fh:
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        hp_host = hp_pair.rstrip("\n").split(":")
        Ip = hp_host[0]
        Port = hp_host[1]
        with open("commands.txt") as fh:
            completed = subprocess.run("ssh " + str(Ip) + " -p " + str(Port) , capture_output=True, text=True, shell=True, stdin=fh)